// **** general options - testing

#define TESTING_ENABLE_ASSERTIONS    1
#define TESTING_ENABLE_STATISTICS    1
#define TESTING_ENABLE_DEBUGGING     1
#define TESTING_ALWAYS_MIGRATE       1 // KOS only
#define TESTING_DISABLE_HEAP_CACHE   1 // KOS only

// **** general options - safer execution

//#define TESTING_DISABLE_ALLOC_LAZY   1 // KOS only
//#define TESTING_DISABLE_DEEP_IDLE    1 // KOS only
//#define TESTING_DISABLE_PREEMPTION   1 // KOS only
//#define TESTING_REPORT_INTERRUPTS    1 // KOS only

// **** general options - alternative design

#define TESTING_LOCKFREE_READYQUEUE  1
//#define TESTING_BLOCKING_LOCKFREE    1
#define TESTING_IDLE_SPIN            1 // spin before idle
#define TESTING_PLACEMENT_RR         1 // use round-robin placement of new fibres/threads
//#define TESTING_WORK_STEALING        1 // enable work stealing (default transient)
//#define TESTING_WORK_STEALING_PERM   1 // enable permanent work stealing

// **** libfibre options - event handling

#define TESTING_POLLER_FIBRES        1 // vs. per-cluster poller pthread
#define TESTING_POLLER_IDLEWAIT      1 // vs. wake up poller anytime
#define TESTING_POLLER_IDLETIMEDWAIT 1 // vs. wait indefinitely for idle
//#define TESTING_TIMER_SIGNAL         1 // fire timers via signal (instead of epoll)
#define TESTING_RESPONSIVE_TIMER     1 // improve timer latency (vs. throughput)
//#define TESTING_RESPONSIVE_ALL       1 // improve overall latency (vs. throughput)
#define TESTING_BULK_RESUME          1 // vs. individual resume

// **** KOS console/serial output configuration

//#define TESTING_DEBUG_STDOUT         1
#define TESTING_STDOUT_DEBUG         1
#define TESTING_STDERR_DEBUG         1

// **** KOS-specific tests

//#define TESTING_KEYCODE_LOOP         1
//#define TESTING_LOCK_TEST            1
#define TESTING_TCP_TEST             1
#define TESTING_MEMORY_HOG           1
#define TESTING_PING_LOOP            1
//#define TESTING_TIMER_TEST           1
