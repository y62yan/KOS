/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Cluster.h"
#include "runtime/RuntimeImpl.h"

// BG enqueue -> find idle "real" processor to run stack
// idleLock synchronizes between 'addIdleProcessor' and 'findIdleProcessor'
void VirtualProcessor::wakeUp() {
  BaseProcessor* idleProc = cluster->findIdleProcessor();
  GENASSERT1(idleProc != this, FmtHex(this));
  if (idleProc) idleProc->wakeUp();
}

void BaseProcessor::setCluster(Cluster& c) {
  cluster = &c;
  cluster->addProcessor(*this);
}

static const unsigned int spinMax = 32;

bool BaseProcessor::findWork() {
restart:
  if (StackContext::idleYieldFirst(_friend<BaseProcessor>())) goto restart;
#if TESTING_IDLE_SPIN
  for (unsigned int spin = 1; spin < spinMax; spin += 1) {
    if (StackContext::idleYieldSpin(_friend<BaseProcessor>())) goto restart;
  }
#endif
  // check for termination
  if (terminate && empty()) {
    cluster->removeProcessor(*this);
    return false;
  }
  // try to borrow or steal
  if (StackContext::idleYieldLast(_friend<BaseProcessor>())) goto restart;
  // report idle and wait
  return true;
}

inline bool BaseProcessor::testScheduleCount(size_t factor) {
  if (++scheduleCounter >= factor * cluster->procCount()) {
    scheduleCounter = 0;
    return true;
  } else {
    return false;
  }
}

inline bool BaseProcessor::tryDequeue(StackContext*& s) {
#if TESTING_WORK_STEALING
  s = readyQueue.dequeueSafe();
#else
  s = readyQueue.dequeue();
#endif
  if fastpath(s) {
    stats->deq.count();
    return true;
  }
  return false;
}

inline bool BaseProcessor::tryStage(StackContext*& s) {
  s = cluster->stage();
  if slowpath(s) { // staging expected to happen rarely
    stats->stage.count();
    s->setResumeProcessor(*this, _friend<BaseProcessor>());
    return true;
  }
  return false;
}

inline bool BaseProcessor::trySteal(StackContext*& s) {
#if TESTING_WORK_STEALING
  BaseProcessor* p = cluster->steal();
  if fastpath(p && p->load() > 1) {
    s = p->readyQueue.dequeueSafe();
    if fastpath(s) {
#if defined(__KOS__)
      if slowpath(s->getAffinity()) {
        p->readyQueue.singleEnqueue(*s);
        return false;
      }
#endif
#if TESTING_WORK_STEALING_PERM
      if (p->load() > 5) {
        s->setResumeProcessor(*this, _friend<BaseProcessor>());
        stats->move.count();
      } else
#endif
      stats->steal.count();
      return true;
    }
  }
#endif
  return false;
}

inline bool BaseProcessor::tryBorrow(StackContext*& s, size_t maxPrio) {
  s = cluster->borrow(maxPrio);
  if fastpath(s) {
    stats->borrow.count();
    return true;
  }
  return false;
}

StackContext* BaseProcessor::schedule(_friend<StackContext> fs) {
  StackContext* nextStack;
#if TESTING_RESPONSIVE_ALL
  if (testScheduleCount(32)) {
    if slowpath(tryStage(nextStack)) return nextStack;
    if fastpath(tryBorrow(nextStack)) return nextStack;
  }
#elif TESTING_RESPONSIVE_TIMER
  if (testScheduleCount(2)) {
    if fastpath(tryBorrow(nextStack, topPriority)) return nextStack;
  }
#endif
  if fastpath(tryDequeue(nextStack)) return nextStack;
  if slowpath(tryStage(nextStack)) return nextStack;
  return idleStack;
}

StackContext* BaseProcessor::scheduleIdleSpin(_friend<StackContext>) {
  StackContext* nextStack;
  if fastpath(tryDequeue(nextStack)) return nextStack;
  return idleStack;
}

StackContext* BaseProcessor::scheduleIdleLast(_friend<StackContext>) {
  StackContext* nextStack;
  if fastpath(trySteal(nextStack)) return nextStack;
  if fastpath(tryBorrow(nextStack)) return nextStack;
  return idleStack;
}
