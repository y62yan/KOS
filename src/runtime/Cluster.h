/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Cluster_h_
#define _Cluster_h_ 1

#include "runtime/BaseProcessor.h"

class Cluster {
#if TESTING_PLACEMENT_RR
  SystemLock      ringLock;
  BaseProcessor*  ringProc;
#endif
  volatile size_t pCount;

  VirtualProcessor stagingProc;
  VirtualProcessor backgroundProc;

protected:
  SystemLock      idleLock;
  ProcessorList   idleList;
  volatile size_t idleCount;
  ProcessorList   busyList;

public:
  Cluster() : pCount(0), stagingProc(this, "Staging"), backgroundProc(this, "Background"), idleCount(0) {}

  size_t procCount() const { return pCount; }

  void addProcessor(BaseProcessor& proc) {
#if TESTING_PLACEMENT_RR
    ScopedLock<SystemLock> sls(ringLock);
    if (ringProc == nullptr) {
      ProcessorRing::init(proc);
      ringProc = &proc;
    } else {
      ProcessorRing::insert_after(*ringProc, proc);
    }
#endif
    ScopedLock<SystemLock> sl(idleLock);
    busyList.push_back(proc);
    pCount += 1;
  }

  void removeProcessor(BaseProcessor& proc) {
#if TESTING_PLACEMENT_RR
    ScopedLock<SystemLock> sls(ringLock);
    GENASSERT(ringProc);
    // move ringProc, if necessary
    if (ringProc == &proc) ringProc = ProcessorRing::next(*ringProc);
    // ring empty?
    if (ringProc == &proc) ringProc = nullptr;
    ProcessorRing::remove(proc);
#endif
    ScopedLock<SystemLock> sl(idleLock);
    busyList.remove(proc);
    pCount -= 1;
  }

  VirtualProcessor& placement(_friend<StackContext>, bool bg = false) {
    if slowpath(bg) return backgroundProc;
#if TESTING_PLACEMENT_RR
    ScopedLock<SystemLock> sl(ringLock);
    GENASSERT(ringProc);
    ringProc = ProcessorRing::next(*ringProc);
    return *ringProc;
#else
    return stagingProc;
#endif
  }

  VirtualProcessor& restage(_friend<StackContext>) {
    return stagingProc;
  }

  StackContext* stage() {
    return stagingProc.dequeueSafe(_friend<Cluster>());
  }

  BaseProcessor* steal() {
    ScopedLock<SystemLock> sl(idleLock);
    return busyList.empty() ? nullptr : busyList.front();
  }

  StackContext* borrow(size_t maxPrio = maxPriority) {
    return backgroundProc.dequeueSafe(_friend<Cluster>(), maxPrio);
  }

  size_t addIdleProcessor(BaseProcessor& proc) {
    ScopedLock<SystemLock> sl(idleLock);
    if (backgroundProc.load() || stagingProc.load()) return 0;
    busyList.remove(proc);
    idleList.push_back(proc);
    idleCount += 1;
    Runtime::debugS("Processor ", FmtHex(&proc), " on idle list");
    return idleCount;
  }

  void removeIdleProcessor(BaseProcessor& proc) {
    ScopedLock<SystemLock> sl(idleLock);
    idleCount -= 1;
    idleList.remove(proc);
    busyList.push_back(proc);
    Runtime::debugS("Processor ", FmtHex(&proc), " off idle list");
  }

  BaseProcessor* findIdleProcessor() {
    ScopedLock<SystemLock> sl(idleLock);
    return idleList.empty() ? nullptr : idleList.front();
  }
};

#endif /* _Cluster_h_ */
