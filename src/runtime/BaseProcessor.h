/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _BaseProcessor_h_
#define _BaseProcessor_h_ 1

#include "runtime/StackContext.h"

class BasePoller;
class Cluster;

class ReadyQueue {
  typedef SystemLock ReadyLock;
  ReadyLock  readyLock;
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
  BlockingStackMPSC queue;
#else
  StackMPSC  queue[numPriority];
#endif
#else /* TESTING_LOCKFREE_READYQUEUE */
  StackQueue queue[numPriority];
#endif

  volatile size_t count;

  ReadyQueue(const ReadyQueue&) = delete;            // no copy
  ReadyQueue& operator=(const ReadyQueue&) = delete; // no assignment

public:
  StackContext* dequeue(size_t maxPrio = maxPriority) {
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
    StackContext* s = queue.pop();
    if fastpath(s) {
      __atomic_sub_fetch(&count, 1, __ATOMIC_RELAXED);
      return s;
    }
#else
    while (count > 0) {                      // spin until push completes
      for (mword p = 0; p <= maxPrio; p += 1) {
        StackContext* s = queue[p].pop();
        if (s) {
          __atomic_sub_fetch(&count, 1, __ATOMIC_RELAXED);
          return s;
        } else if (maxPrio < maxPriority) {
          return nullptr;
        }
      }
    }
#endif /* TESTING_BLOCKING_LOCKFREE */
#else
    ScopedLock<ReadyLock> sl(readyLock);
    for (mword p = 0; p <= maxPrio; p += 1) {
      if (!queue[p].empty()) {
        count -= 1;
        return queue[p].pop();
      }
    }
#endif
    return nullptr;
  }

  StackContext* dequeueSafe(size_t maxPrio = maxPriority) {
#if TESTING_LOCKFREE_READYQUEUE
    ScopedLock<ReadyLock> sl(readyLock);
#endif
    return dequeue(maxPrio);
  }

  bool singleEnqueue(StackContext& s) {
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
    __atomic_add_fetch(&count, 1, __ATOMIC_RELAXED);
    return queue.push(s);
#else
    queue[s.getPriority()].push(s);
    return __atomic_add_fetch(&count, 1, __ATOMIC_RELAXED) == 1;
#endif /* TESTING_BLOCKING_LOCKFREE */
#else
    ScopedLock<ReadyLock> sl(readyLock);
    queue[s.getPriority()].push(s);
    count += 1;
    return count == 1;
#endif
  }

  bool bulkEnqueue(ResumeQueue& rq) {
    GENASSERT(rq.count);
#if TESTING_LOCKFREE_READYQUEUE
#if TESTING_BLOCKING_LOCKFREE
    __atomic_add_fetch(&count, rq.count, __ATOMIC_RELAXED);
    return queue.transferAllFrom(rq.queue);
#else
    bool retcode = (__atomic_fetch_add(&count, rq.count, __ATOMIC_RELAXED) == 0);
    for (mword p = 0; p <= maxPriority; p += 1) {
      queue[p].transferAllFrom(rq.queue[p]);
    }
    return retcode;
#endif /* TESTING_BLOCKING_LOCKFREE */
#else
    ScopedLock<ReadyLock> sl(readyLock);
    for (mword p = 0; p <= maxPriority; p += 1) {
      queue[p].transferAllFrom(rq.queue[p]);
    }
    count += rq.count;
    return count == rq.count;
#endif
  }

public:
  ReadyQueue() : count(0) {}
  size_t load() const { return count; }
};

class VirtualProcessor {
protected:
  ReadyQueue      readyQueue;
  Cluster*      	cluster;
  ProcessorStats* stats;

public:
  VirtualProcessor(Cluster* c, const char* n) : cluster(c) {
    stats = new ProcessorStats(this, n);
  }

  size_t load() const { return readyQueue.load(); }

  virtual void wakeUp();

  void singleResume(StackContext& s, _friend<StackContext>) {
    GENASSERT1(s.getPriority() <= maxPriority, s.getPriority());
    Runtime::debugS("Stack ", FmtHex(&s), " queueing on ", FmtHex(this));
    stats->enq.count();
    if (readyQueue.singleEnqueue(s)) wakeUp();
  }

  // No locking for ResumeQueue! The caller better knows what it is doing.
  void bulkResume(ResumeQueue& rq, _friend<BasePoller>) {
    Runtime::debugS(rq.count, " stacks bulk queueing on ", FmtHex(this));
    stats->bulk.add(rq.count);
    if (readyQueue.bulkEnqueue(rq)) wakeUp();
  }

  StackContext* dequeueSafe(_friend<Cluster>, size_t maxPrio = maxPriority) {
    return readyQueue.dequeueSafe(maxPrio);
  }

  StackContext* dequeue(_friend<Cluster>, size_t maxPrio = maxPriority) {
    return readyQueue.dequeue(maxPrio);
  }
};

class BaseProcessor;
typedef IntrusiveRing<BaseProcessor,0,2> ProcessorRing;
typedef IntrusiveList<BaseProcessor,1,2> ProcessorList;

class BaseProcessor : public ProcessorList::Link, public VirtualProcessor {
  inline bool testScheduleCount(size_t factor);
  inline bool tryDequeue(StackContext*& s);
  inline bool tryStage(StackContext*& s);
  inline bool trySteal(StackContext*& s);
  inline bool tryBorrow(StackContext*& s, size_t maxPrio = maxPriority);

protected:
  StackContext*   idleStack;
  volatile size_t stackWatch;
  size_t          scheduleCounter;
  volatile bool   terminate;

  void setupIdle(StackContext* is) {
    idleStack = is;
    idleStack->setPriority(idlePriority);
  }

  bool findWork();
  void setCluster(Cluster& c);

public:
  BaseProcessor(Cluster* c = nullptr) : VirtualProcessor(c, "Processor"),
    idleStack(nullptr), stackWatch(0), scheduleCounter(0), terminate(false) {}

  Cluster& getCluster() { GENASSERT(cluster); return *cluster; }

  bool empty() const { return !load() && !stackWatch; }

  void addStackWatch(VirtualProcessor* p, _friend<StackContext>) {
    stackWatch += (int)(bool)(this == p);
  }
  void delStackWatch(VirtualProcessor* p, _friend<StackContext>) {
    stackWatch -= (int)(bool)(this == p);
  }

  StackContext* schedule(_friend<StackContext>);
  StackContext* scheduleIdleSpin(_friend<StackContext>);
  StackContext* scheduleIdleLast(_friend<StackContext>);
};

#endif /* _BaseProcessor_h_ */
