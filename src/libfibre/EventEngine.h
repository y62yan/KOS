/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _EventEngine_h_
#define _EventEngine_h_ 1

#include "libfibre/SystemProcessor.h"

#include <unistd.h>       // close
#include <sys/resource.h> // getrlimit
#include <sys/types.h>
#include <sys/socket.h>
#if TESTING_TIMER_SIGNAL
#include <signal.h>
#endif

class EventEngine {
  typedef FifoMutex<BinaryLock<>>             EventLock;
  typedef FifoSemaphore<BinaryLock<1,1>,true> EventSemaphore; // only two competitors, no backoff needed

  // A vector for FDs works well here in principle, because POSIX guarantees lowest-numbered FDs:
  // http://pubs.opengroup.org/onlinepubs/9699919799/functions/V2_chap02.html#tag_15_14
  // A fixed-size array based on 'getrlimit' is somewhat brute-force, but simple and fast.
  struct SyncIO {
    EventLock      lock;
    EventSemaphore sem;
    void reset() { lock.acquire(); sem.reset(); lock.release(); }
  };

  struct SyncRW {
    SyncIO RD;
    SyncIO WR;
  } *fdSyncVector;

  // file operations are not considered blocking in terms of select/poll/epoll
  // therefore, all file operations are executed on dedicated Cluster/SP(s)
  FibreCluster    ioCluster;
  SystemProcessor ioProcessor;

#if TESTING_TIMER_SIGNAL
  EventSemaphore timerSem;
  timer_t        timerSignal;
  static void    timerHandler(int);
#else
  int            timerFD;
#endif
  Fibre*         timerFibre;
  volatile bool  timerTerminate;
  inline void    timerLoop();
  static void    timerLoopSetup(void*);

#if TESTING_POLLER_FIBRES || !TESTING_TIMER_SIGNAL
  PollerThread   masterPoller;                // runs without cluster
#endif

public:
  EventEngine() : fdSyncVector(0), ioCluster(0), ioProcessor(ioCluster), timerTerminate(false) {
    struct rlimit rl;
    SYSCALL(getrlimit(RLIMIT_NOFILE, &rl));  // get hard limit for file descriptor count
    unsigned long fdcount = rl.rlim_max;
#if TESTING_TIMER_SIGNAL
    stack_t ss;
    ss.ss_flags = 0;
    ss.ss_sp = mmap(0, minimumStackSize, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
    GENASSERT(ss.ss_sp != MAP_FAILED);
    ss.ss_size = minimumStackSize;
    SYSCALL(sigaltstack(&ss, nullptr));
    struct sigaction sa;
    sa.sa_handler = timerHandler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_ONSTACK | SA_RESTART;
    SYSCALL(sigaction(SIGALRM, &sa, 0));
    SYSCALL(timer_create(CLOCK_REALTIME, nullptr, &timerSignal));
#else
#if __FreeBSD__
    timerFD = fdcount;
    fdcount += 1;
#else
    timerFD = SYSCALLIO(timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK | TFD_CLOEXEC));
#endif
#endif // TESTING_TIMER_SIGNAL
    fdSyncVector = new SyncRW[fdcount];      // create vector of R and W sync points
  }

  ~EventEngine() {
    timerTerminate = true;                   // set timer loop termination flag
    notifyTimeout({0,0});                    // wake up timer loop
    delete timerFibre;
#if TESTING_TIMER_SIGNAL
    SYSCALL(timer_delete(timerSignal));
#endif
    delete[] fdSyncVector;
  }

  void startTimerHandling() {
    timerFibre = new Fibre(CurrCluster(), defaultStackSize, true);
    timerFibre->setPriority(topPriority);
    timerFibre->run(timerLoopSetup, this);
  }

  void notifyTimeout(const Time& timeout) {
#if TESTING_TIMER_SIGNAL
    itimerspec tval = { {0,0}, timeout };
    SYSCALL(timer_settime(timerSignal, 0, &tval, nullptr));
#else
    masterPoller.setTimer(timerFD, timeout);
#endif // TESTING_TIMER_SIGNAL
  }

  void registerSyncFD(int fd) {
    CurrPoller().registerFD(fd);
  }
  void deregisterSyncFD(int fd) {
    fdSyncVector[fd].RD.reset();
    fdSyncVector[fd].WR.reset();
  }
  void suspendFD(int fd) {
    fdSyncVector[fd].RD.sem.P_fake();
    fdSyncVector[fd].WR.sem.P_fake();
  }
  void resumeFD(int fd) {
    fdSyncVector[fd].RD.sem.V();
    fdSyncVector[fd].WR.sem.V();
  }

  template<bool Input>
  void block(int fd) {
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    sync.sem.P();
  }

  template<bool Input>
  void unblock(int fd, ProcessorResumeSet& procSet, _friend<BasePoller>) {
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
#if TESTING_BULK_RESUME
    sync.sem.V_bulk(procSet);
#else
    sync.sem.V();
#endif
  }

#if TESTING_POLLER_FIBRES
  void registerPollFD(int fd) {
    masterPoller.setupPollFD<false>(fd);
  }
  void blockPollFD(int fd) {
    masterPoller.setupPollFD<true>(fd);
    fdSyncVector[fd].RD.sem.P();
  }
  void unblockPollFD(int fd, _friend<Poller>) {
    fdSyncVector[fd].RD.sem.V();
  }
#endif

  template<typename T, class... Args>
  T directIO(T (*iofunc)(Args...), Args... a) {
    VirtualProcessor& proc = Fibre::migrateSelf(ioCluster, _friend<EventEngine>());
    int result = iofunc(a...);
    Fibre::migrateSelf(proc, _friend<EventEngine>());
    return result;
  }

  template<bool Input, bool Yield, typename T, class... Args>
  T syncIO( T (*iofunc)(int, Args...), int fd, Args... a) {
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    if (Yield) Fibre::yield();
    sync.lock.acquire();
    for (;;) {
      T ret = iofunc(fd, a...);
      if (ret >= 0 || errno != EAGAIN) {
        sync.lock.release();
        return ret;
      }
      sync.sem.P();
    }
  }
};

extern EventEngine* _lfEventEngine;

// input: yield before network read
template<typename T, class... Args>
T lfInput( T (*readfunc)(int, Args...), int fd, Args... a) {
  return _lfEventEngine->syncIO<true,true>(readfunc, fd, a...);
}

// output: no yield before write
template<typename T, class... Args>
T lfOutput( T (*writefunc)(int, Args...), int fd, Args... a) {
  return _lfEventEngine->syncIO<false,false>(writefunc, fd, a...);
}

// direct I/O
template<typename T, class... Args>
T lfDirectIO( T (*iofunc)(int, Args...), int fd, Args... a) {
  return _lfEventEngine->directIO(iofunc, fd, a...);
}

// socket: register SOCK_DGRAM fd for I/O synchronization; SOCK_STREAM later (cf. listen, connect)
static inline int lfSocket(int domain, int type, int protocol) {
  int fd = socket(domain, type | SOCK_NONBLOCK, protocol);
  if (fd >= 0 && type != SOCK_STREAM) _lfEventEngine->registerSyncFD(fd);
  return fd;
}

// POSIX says that bind might fail with EINPROGRESS, but not on Linux...
static inline int lfBind(int fd, const sockaddr *addr, socklen_t addrlen) {
  int ret = bind(fd, addr, addrlen);
  if (ret < 0 && errno == EINPROGRESS) {
    _lfEventEngine->block<true>(fd);
    socklen_t sz = sizeof(ret);
    SYSCALL(getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &sz));
  }
  return ret;
}

// listen: can register SOCK_STREAM fd only after 'listen' system call on FreeBSD
static inline int lfListen(int fd, int backlog) {
  int ret = listen(fd, backlog);
  _lfEventEngine->registerSyncFD(fd);
  return ret;
}

// accept: register new file descriptor for I/O events, no yield before accept
static inline int lfAccept(int fd, sockaddr *addr, socklen_t *addrlen, int flags = 0) {
  int ret = _lfEventEngine->syncIO<true,false>(accept4, fd, addr, addrlen, flags | SOCK_NONBLOCK);
  if (ret >= 0) _lfEventEngine->registerSyncFD(ret);
  return ret;
}

// see man 3 connect for EINPROGRESS; register SOCK_STREAM fd now (FreeBSD, cf. listen)
static inline int lfConnect(int fd, const sockaddr *addr, socklen_t addrlen) {
  _lfEventEngine->registerSyncFD(fd);
  int ret = connect(fd, addr, addrlen);
  if (ret < 0 && errno == EINPROGRESS) {
    _lfEventEngine->block<false>(fd);
    socklen_t sz = sizeof(ret);
    SYSCALL(getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &sz));
  }
  return ret;
}

// shutdown: deregister file descriptor -> reset RD/WR synchronization
static inline int lfShutdown(int fd, int how) {
  _lfEventEngine->deregisterSyncFD(fd);
  return shutdown(fd, how);
}

// close: deregister file descriptor -> reset RD/WR synchronization
static inline int lfClose(int fd) {
  _lfEventEngine->deregisterSyncFD(fd); // fd might be reused after close
  return close(fd);
}

// dup: duplicate file descriptor -> not necessarily a good idea on Linux - think twice about it!
static inline int lfDup(int fd) {
  int d = dup(fd);
  _lfEventEngine->registerSyncFD(d);
  return d;
}

#endif /* _EventEngine_h_ */
