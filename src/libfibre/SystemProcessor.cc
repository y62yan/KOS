/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "libfibre/SystemProcessor.h"

#include <limits.h>       // PTHREAD_STACK_MIN

SystemProcessor::SystemProcessor() : SystemProcessor(CurrCluster()) {}

SystemProcessor::SystemProcessor(Cluster& cluster) {
  setCluster(cluster);
  pthread_attr_t attr;                 // create pthread running idleLoop
  SYSCALL(pthread_attr_init(&attr));
  SYSCALL(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED));
#if __linux__ // FreeBSD jemalloc segfaults when trying to use minimum stack
  SYSCALL(pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN));
#endif
  SYSCALL(pthread_create(&sysThread, &attr, idleLoopSetupPthread, this));
  SYSCALL(pthread_attr_destroy(&attr));
}

SystemProcessor::SystemProcessor(Cluster& cluster, funcvoid1_t func, ptr_t arg) {
  setCluster(cluster);
  sysThread = pthread_self();
  Context::currProc = this;
  // idle loop takes over pthread stack - create fibre without stack
  setupIdle(new Fibre(_friend<SystemProcessor>()));
  Context::currStack = idleStack;
  // tmp fibre will run first on this new SP
  Fibre* tmpFibre = new Fibre(defaultStackSize, _friend<SystemProcessor>());
  tmpFibre->run(func, arg);
  ((Fibre*)idleStack)->runDirect(idleLoopSetupFibre, this, _friend<SystemProcessor>());
}

SystemProcessor::SystemProcessor(Cluster& cluster, _friend<_Bootstrapper>) {
  setCluster(cluster);
  sysThread = pthread_self();          // main pthread runs main routine
  Context::currProc = this;
}

Fibre* SystemProcessor::init(_friend<_Bootstrapper>) {
  // create proper idle loop fibre
  setupIdle(new Fibre(defaultStackSize, _friend<SystemProcessor>()));
  idleStack->setup((ptr_t)idleLoopSetupFibre, this);
  // main fibre takes over pthread stack - create fibre without stack
  Fibre* mainFibre = new Fibre(_friend<SystemProcessor>());
  Context::currStack = mainFibre;
  return mainFibre;
}

SystemProcessor::~SystemProcessor() {
  terminate = true;                 // no more work added, idle terminating
  // make sure current fibre is off this processor
  if (this == &CurrProcessor()) Fibre::migrateSelf(*cluster);
  GENASSERT(this != &CurrProcessor());
  wakeUp();                         // wake up idle loop (just in case)
  delete (Fibre*)idleStack;         // wait for idle loop to finish
}

inline void SystemProcessor::idleLoop() {
  for (;;) {
    if (!findWork()) break;
    // report idle and wait, if enough spins have passed
    if (reinterpret_cast<FibreCluster*>(cluster)->addIdleProcessor(*this)) {
      stats->idle.count();
      idleSem.P();
      cluster->removeIdleProcessor(*this);
    }
  }
}

void SystemProcessor::idleLoopSetupFibre(void* sp) {
  reinterpret_cast<SystemProcessor*>(sp)->idleLoop();
}

void* SystemProcessor::idleLoopSetupPthread(void* sp) {
  SystemProcessor* This = reinterpret_cast<SystemProcessor*>(sp);
  Context::currProc = This;
  // idle loop takes over pthread stack - create fibre without stack
  This->setupIdle(new Fibre(_friend<SystemProcessor>()));
  Context::currStack = This->idleStack;
  ((Fibre*)This->idleStack)->runDirect(idleLoopSetupFibre, This, _friend<SystemProcessor>());
  return nullptr;
}
