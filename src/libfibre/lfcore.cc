/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "libfibre/Fibre.h"
#include "libfibre/EventEngine.h"

// debug output lock
SystemLock* _lfDebugOutputLock = nullptr;                            // lfbasics.h

// instance definitions for Context members
thread_local SystemProcessor* volatile Context::currProc  = nullptr; // lfbasics.h
thread_local StackContext*    volatile Context::currStack = nullptr; // lfbasics.h

// noinline routines for Context
void Context::setCurrStack(StackContext& s, _friend<Runtime>) { currStack = &s; }
SystemProcessor* Context::self()       { return currProc; }
StackContext*    Context::CurrStack()  { return currStack; }

// global pointers
EventEngine*     _lfEventEngine    = nullptr; // EventEngine.h
TimerQueue*      defaultTimerQueue = nullptr; // BlockingSync.h

#if TESTING_ENABLE_DEBUGGING
SystemLock*      _globalStackLock  = nullptr; // StackContext.h
GlobalStackList* _globalStackList  = nullptr; // StackContext.h
#endif

// make these pointers global static to enable gdb access
static Fibre*           _lfMainFibre     = nullptr;
static SystemProcessor* _lfMainProcessor = nullptr;
static PollerCluster*   _lfMainCluster = nullptr;

#if TESTING_ENABLE_STATISTICS
std::list<StatsObject*>* StatsObject::lst = nullptr;
#endif

#if TESTING_ENABLE_ASSERTIONS
static SystemLock _abortLock;
void _SYSCALLabortLock() { _abortLock.acquire(); }
void _SYSCALLabortUnlock() { _abortLock.release(); }
#endif

// ******************** EVENT POLLING ********************

// drain all events with non-blocking epoll_wait
inline void BasePoller::poll(int evcnt) {
  for (;;) {
    stats->events.add(evcnt);
    ProcessorResumeSet procSet;
    for (int e = 0; e < evcnt; e += 1) {
#if __FreeBSD__
      struct kevent& ev = events[e];
      if (ev.filter == EVFILT_READ || ev.filter == EVFILT_TIMER) {
        _lfEventEngine->unblock<true>(ev.ident, procSet, _friend<BasePoller>());
      } else if (ev.filter == EVFILT_WRITE) {
        _lfEventEngine->unblock<false>(ev.ident, procSet, _friend<BasePoller>());
      }
#else // __linux__ below
      epoll_event& ev = events[e];
      if (ev.events & (EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP)) {
        _lfEventEngine->unblock<true>(ev.data.fd, procSet, _friend<BasePoller>());
      }
      if (ev.events & (EPOLLOUT | EPOLLRDHUP | EPOLLERR)) {
        _lfEventEngine->unblock<false>(ev.data.fd, procSet, _friend<BasePoller>());
      }
#endif
    }
    for (std::pair<VirtualProcessor* const,ResumeQueue>& p : procSet) {
      p.first->bulkResume(p.second, _friend<BasePoller>());
    }
    if (evcnt < maxPoll) break;
#if __FreeBSD__
    static const timespec ts = Time::zero();
    evcnt = kevent(pollFD, nullptr, 0, events, maxPoll, &ts);
#else // __linux__ below
    evcnt = epoll_wait(pollFD, events, maxPoll, 0);
#endif
    GENASSERT1(evcnt >= 0 || errno == EINTR, errno); // gracefully handle EINTR
  }
}

template<typename T>
inline void PollerThread::pollLoop() {
  while (!pollTerminate) {
    stats->blocks.count();
#if !TESTING_POLLER_FIBRES || TESTING_RESPONSIVE_TIMER || TESTING_RESPONSIVE_ALL
    if (!reinterpret_cast<T*>(this)->wait(_friend<PollerThread>())) usleep(100);  // avoid trickle loop
#endif
#if __FreeBSD__
    int evcnt = kevent(pollFD, nullptr, 0, events, maxPoll, nullptr); // blocking
#else // __linux__ below
    int evcnt = epoll_wait(pollFD, events, maxPoll, -1);              // blocking
#endif
    if (paused) pauseSem.P();
    if (evcnt >= 0) poll(evcnt);
    else { GENASSERT1(errno == EINTR, errno); } // gracefully handle EINTR
  }
}

void* PollerThread::pollLoopSetup(void* This) {
  reinterpret_cast<PollerThread*>(This)->pollLoop<PollerThread>();
  return nullptr;
}

#if TESTING_POLLER_FIBRES
Poller::Poller(PollerCluster& cluster) : BasePoller("PollerFibre") {
  pollFibre = new Fibre(cluster, defaultStackSize, true);
  pollFibre->setPriority(lowPriority);
  pollFibre->run(pollLoopSetup, this);
}

void Poller::stop() {
  pollTerminate = true; // set termination flag, then unblock -> terminate
  _lfEventEngine->unblockPollFD(pollFD, _friend<Poller>());
  delete pollFibre;
}

inline void Poller::pollLoop() {
  _lfEventEngine->registerPollFD(pollFD);
  while (!pollTerminate) {
    stats->blocks.count();
    _lfEventEngine->blockPollFD(pollFD);
#if __FreeBSD__
    static const timespec ts = Time::zero();
    int evcnt = kevent(pollFD, nullptr, 0, events, maxPoll, &ts);
#else // __linux__ below
    int evcnt = epoll_wait(pollFD, events, maxPoll, 0);
#endif
    if (paused) pauseSem.P();
    if (evcnt >= 0) poll(evcnt);
    else { GENASSERT1(errno == EINTR, errno); } // gracefully handle EINTR
  }
}

void Poller::pollLoopSetup(void* This) {
  reinterpret_cast<Poller*>(This)->pollLoop();
}

#else // TESTING_POLLER_FIBRES

void* Poller::pollLoopSetup(void* This) {
  reinterpret_cast<Poller*>(This)->pollLoop<Poller>();
  return nullptr;
}

inline bool Poller::wait(_friend<PollerThread>) {
  return cluster.waitForPoll();
}

#endif // TESTING_POLLER_FIBRES

// ******************** TIMER HANDLING ********************

#if TESTING_TIMER_SIGNAL
void EventEngine::timerHandler(int) {
  if (_lfEventEngine->timerSem.tryV()) return;
  // if V not possible (locked), reschedule
  _lfNotifyTimeout( {0,0} );
}
#endif

inline void EventEngine::timerLoop() {
#if !TESTING_TIMER_SIGNAL
#if __linux__
  masterPoller.registerFD(timerFD);
#endif
#endif // !TESTING_TIMER_SIGNAL
  while (!timerTerminate) {
#if TESTING_TIMER_SIGNAL
    timerSem.P();
#else
#if __FreeBSD__
    block<true>(timerFD);
#else // __linux__ below
    uint64_t count;
    SYSCALLIO(lfInput(read, timerFD, (void*)&count, sizeof(count)));
#endif
#endif // TESTING_TIMER_SIGNAL
    Time currTime;
    SYSCALL(clock_gettime(CLOCK_REALTIME, &currTime));
    defaultTimerQueue->checkExpiry(currTime);
  }
#if !TESTING_TIMER_SIGNAL
#if __linux__
  close(timerFD);
#endif
#endif // !TESTING_TIMER_SIGNAL
}

void EventEngine::timerLoopSetup(void* This) {
  reinterpret_cast<EventEngine*>(This)->timerLoop();
}

void _lfNotifyTimeout(const Time& timeout) {
  _lfEventEngine->notifyTimeout(timeout);
}

// ******************** BOOTSTRAP ********************

// bootstrap counter definition
std::atomic<int> _Bootstrapper::counter(0);

_Bootstrapper::_Bootstrapper() {
  if (++counter == 1) {
#if TESTING_ENABLE_STATISTICS
    StatsObject::lst = new std::list<StatsObject*>;
#endif
    // create lock for debug output
    _lfDebugOutputLock = new SystemLock;
#if TESTING_ENABLE_DEBUGGING
    // create global fibre list
    _globalStackLock = new SystemLock;
    _globalStackList = new GlobalStackList;
#endif
    // create default timer queue
    defaultTimerQueue = new TimerQueue;
    // start event demultiplexing
    _lfEventEngine = new EventEngine;
    // create default cluster -> includes poller
    _lfMainCluster = new PollerCluster;
    // create main SP
    _lfMainProcessor = new SystemProcessor(*_lfMainCluster, _friend<_Bootstrapper>());
    // create main fibre and main SP's idle fibre using dedicated interface
    _lfMainFibre = _lfMainProcessor->init(_friend<_Bootstrapper>());
    // start timer handling in event engine -> needs main cluster/processor
    _lfEventEngine->startTimerHandling();
  }
}

_Bootstrapper::~_Bootstrapper() {
  if (--counter == 0) {
//    delete _lfMainFibre;
//    delete _lfMainProcessor;
//    delete _lfMainCluster;
//    delete _lfEventEngine;
//    delete defaultTimerQueue;
    delete _lfDebugOutputLock;
#if TESTING_ENABLE_DEBUGGING
    delete _globalStackList;
    delete _globalStackLock;
#endif
#if TESTING_ENABLE_STATISTICS
    StatsObject::printAll(std::cout);
    delete StatsObject::lst;
#endif
  }
}
