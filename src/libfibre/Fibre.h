/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Fibre_h_
#define _Fibre_h_ 1

#include "runtime/BlockingSync.h"
#include "libfibre/SystemProcessor.h"

#include <sys/mman.h>

static inline ptr_t stackAlloc(size_t size) {
  // check that requested size is a multiple of page size
  GENASSERT1(aligned(size, stackProtection), size);
  // reserve/map size + protection
  ptr_t ptr = mmap(0, size + stackProtection, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
  GENASSERT(ptr != MAP_FAILED);
  // set up protection page
  SYSCALL(mprotect(ptr, stackProtection, PROT_NONE));
  return ptr_t(uintptr_t(ptr) + stackProtection);
}

static inline void stackFree(ptr_t ptr, size_t size) {
  SYSCALL(munmap(ptr_t(uintptr_t(ptr) - stackProtection), size + stackProtection));
}

class StackPool {
  const size_t Size;
  SystemLock lock;
  struct Free : public IntrusiveQueueMPSC<Free>::Link {};
  IntrusiveQueueMPSC<Free> queue;
public:
  StackPool(size_t sz = defaultStackSize) : Size(sz) {}
  ~StackPool() { while (Free* f = queue.pop()) stackFree(f, Size); }
  size_t size() const { return Size; }
  ptr_t alloc(bool unsafe = false) {
    if (unsafe || lock.tryAcquire()) {
      Free* f = queue.pop();
      if (!unsafe) lock.release();
      if fastpath(f) return f;
    }
    return stackAlloc(Size);
  }
  void release(ptr_t p) {
    Free* f = (Free*)p;
    queue.push(*f);
  }
};

class Fibre : public StackContext {
  FloatingPointFlags    fp;          // FP context
  ptr_t                 stackBottom; // bottom of allocated memory for stack
  size_t                stackSize;   // stack size
  StackPool*            stackPool;
  SyncPoint<SystemLock> done;        // synchronization (join) at destructor

  Fibre(PollerCluster& cluster, bool bg, ptr_t sb, size_t sz, StackPool* pool)
  : StackContext(cluster, bg), stackBottom(sb), stackSize(sz), stackPool(pool), done(SyncPoint<SystemLock>::Detached) {
    StackContext::initStackPointer((vaddr)stackBottom + stackSize);
#if TESTING_ENABLE_DEBUGGING
    ScopedLock<SystemLock> sl(*_globalStackLock);
    _globalStackList->push_back(*this);
#endif
  }

  Fibre(SystemProcessor& sp, ptr_t sb, size_t sz, StackPool* pool)
  : StackContext(sp), stackBottom(sb), stackSize(sz), stackPool(pool), done(SyncPoint<SystemLock>::Detached) {
    StackContext::initStackPointer((vaddr)stackBottom + stackSize);
#if TESTING_ENABLE_DEBUGGING
    ScopedLock<SystemLock> sl(*_globalStackLock);
    _globalStackList->push_back(*this);
#endif
  }

public:
  Fibre(size_t sz = defaultStackSize, bool bg = false)
  : Fibre(CurrCluster(), bg, stackAlloc(sz), sz, nullptr) {}

  Fibre(PollerCluster& cluster, size_t sz = defaultStackSize, bool bg = true)
  : Fibre(cluster, bg, stackAlloc(sz), sz, nullptr) {}

  Fibre(StackPool& pool, bool unsafe = false)
  : Fibre(CurrCluster(), false, pool.alloc(unsafe), pool.size(), &pool) {}

  // dedicated constructor for SP idle loop with proper stack, no auto-placement
  Fibre(size_t sz, _friend<SystemProcessor>)
  : Fibre(CurrProcessor(), stackAlloc(sz), sz, nullptr) {}

  // dedicated constructor for SP idle loop or main loop (boot SP) on pthread stack, already running
  Fibre(_friend<SystemProcessor>)
  : Fibre(CurrProcessor(), 0, 0, nullptr) {}

  // synchronize at object destruction, detaching not permitted
  ~Fibre() {
    join();
  }
  void join() { done.wait(); }
  void detach() { done.detach(); }

  // callback from StackContext via Runtime after terminal context switch
  void destroy(_friend<Runtime>) {
#if TESTING_ENABLE_DEBUGGING
    ScopedLock<SystemLock> sl(*_globalStackLock);
    GlobalStackList::remove(*this);
#endif
    if (stackSize) {
      if (stackPool) stackPool->release(stackBottom);
      else stackFree(stackBottom, stackSize);
    }
    done.post();
  }

  // fibre start routines
  Fibre* run(funcvoid1_t func, ptr_t p1 = nullptr) {
    done.reset();
    StackContext::start((ptr_t)func, p1);
    return this;
  }
  Fibre* run(funcvoid0_t func) {
    done.reset();
    StackContext::start((ptr_t)func);
    return this;
  }

  // dedicated invocation for SP running idle loop or main loop (boot SP) on pthread stack
  void runDirect(funcvoid1_t func, ptr_t p1, _friend<SystemProcessor>) {
    done.reset();
    func(p1);
    done.post();
  }

  static void usleep(uint64_t usecs) {
    defaultTimerQueue->sleep(Time::fromUS(usecs));
  }

  static void sleep(uint64_t secs) {
    defaultTimerQueue->sleep(Time(secs, 0));
  }

  Fibre* setPriority(mword p) { StackContext::setPriority(p); return this; }

  void deactivate(_friend<Runtime>) { fp.save(); }
  void activate(_friend<Runtime>) { fp.restore(); }
};

static inline Fibre* CurrFibre() {
  return (Fibre*)CurrStack();
}

#endif /* _Fibre_h_ */
