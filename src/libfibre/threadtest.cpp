/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "fibre.h"

#include <iostream>
#include <signal.h>
#include <unistd.h> // getopt

static unsigned int duration = 10;
static unsigned int threads = 2;
static unsigned int fibres = 0;
static unsigned int locks = 1;
static unsigned int work = 10000;
static unsigned int unlocked = 10000;
static bool yield = false;
static bool serial = false;

struct WorkerInfo {
  union {
    pthread_t pid;
    fibre_t   fid;
  };
  volatile unsigned long long counter;
} __caligned;

struct LockInfo {
  union {
    pthread_mutex_t plock;
    fibre_mutex_t*  flock;
  };
  volatile unsigned long long counter;
} __caligned;

SystemProcessor* sproc = nullptr;

static WorkerInfo* winfo = nullptr;
static LockInfo* linfo = nullptr;
static unsigned int ticks = 0;

static volatile bool running = false;

static void alarmHandler(int) {
  ticks += 1;
  if (ticks >= duration) {
    std::cerr << '\r' << std::flush;
    running = false;
  } else {
    std::cerr << '\r' << ticks << std::flush;
  }
}

static union {
  pthread_barrier_t pbar;
  fibre_barrier_t*  fbar;
} starter;

static void usage(const char* prog) {
  std::cerr << "usage: " << prog << " -d <duration (secs)> -t <threads> -f <fibres> -l <locks> -w <work> -u <unlocked work> -y -s" << std::endl;
}

static void opts(int argc, char** argv) {
  for (;;) {
    int option = getopt( argc, argv, "d:t:f:l:w:u:ysh?" );
    if ( option < 0 ) break;
    switch(option) {
    case 'd': duration = atoi(optarg); break;
    case 't': threads = atoi(optarg); break;
    case 'f': fibres = atoi(optarg); break;
    case 'l': locks = atoi(optarg); break;
    case 'w': work = atoi(optarg); break;
    case 'u': unlocked = atoi(optarg); break;
    case 'y': yield = true; break;
    case 's': serial = true; break;
    case 'h':
    case '?':
      usage(argv[0]);
      exit(1);
    default:
      std::cerr << "unknown option -" << (char)option << std::endl;
      usage(argv[0]);
      exit(1);
    }
  }
  if (argc != optind) {
    std::cerr << "unknown argument - " << argv[optind] << std::endl;
    usage(argv[0]);
    exit(1);
  }
}

static void* worker(void* arg) {
  if (fibres) fibre_barrier_wait(starter.fbar);
  else pthread_barrier_wait(&starter.pbar);
  unsigned int num = (uintptr_t)arg;
  unsigned int lck = random() % locks;
  while (running) {
    for (unsigned int i = 0; i < unlocked; i += 1) asm volatile("nop":::"memory");
    if (fibres) SYSCALL(fibre_mutex_lock(linfo[lck].flock));
    else SYSCALL(pthread_mutex_lock(&linfo[lck].plock));
    for (unsigned int i = 0; i < work; i += 1) asm volatile("nop":::"memory");
    winfo[num].counter += 1;
    linfo[lck].counter += 1;
    if (fibres) SYSCALL(fibre_mutex_unlock(linfo[lck].flock));
    else SYSCALL(pthread_mutex_unlock(&linfo[lck].plock));
    if (yield) {
      if (fibres) fibre_yield();
      else pthread_yield();
    }
    if (serial) lck += 1;
    else lck = random();
    lck %= locks;
  }
  return nullptr;
}

int main(int argc, char** argv) {
  // parse command-line arguments
  opts(argc, argv);

  // create thread & lock structures
  unsigned int workers = fibres ? threads * fibres : threads;
  winfo = new WorkerInfo[workers];
  linfo = new LockInfo[locks];

  // create locks
  for (unsigned int i = 0; i < locks; i += 1) {
    if (fibres) linfo[i].flock = new fibre_mutex_t();
    else SYSCALL(pthread_mutex_init(&linfo[i].plock, nullptr));
    linfo[i].counter = 0;
  }

  // set up random number generator
  srandom(time(nullptr));

  // set up start barrier
  if (fibres) starter.fbar = new fibre_barrier_t(workers + 1);
  else SYSCALL(pthread_barrier_init(&starter.pbar, nullptr, workers + 1));

  // set up alarm
  timer_t timer;
  struct sigaction sa;
  sa.sa_handler = alarmHandler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;
  SYSCALL(sigaction(SIGALRM, &sa, 0));
  SYSCALL(timer_create(CLOCK_REALTIME, nullptr, &timer));
  itimerspec tval = { {1,0}, {1,0} };

  // create fibre processors
  if (fibres) sproc = new SystemProcessor[threads - 1];

  // create threads
  for (unsigned int i = 0; i < workers; i += 1) {
    winfo[i].counter = 0;
    if (fibres) SYSCALL(fibre_create(&winfo[i].fid, nullptr, worker, (void*)uintptr_t(i)));
    else SYSCALL(pthread_create(&winfo[i].pid, nullptr, worker, (void*)uintptr_t(i)));
  }

  // start experiment
  running = true;
  SYSCALL(timer_settime(timer, 0, &tval, nullptr));
  if (fibres) fibre_barrier_wait(starter.fbar);
  else pthread_barrier_wait(&starter.pbar);

  // join threads
  for (unsigned int i = 0; i < workers; i += 1) {
    if (fibres) SYSCALL(fibre_join(winfo[i].fid, nullptr));
    else SYSCALL(pthread_join(winfo[i].pid, nullptr));
  }

  // destroy fibre processors
  if (fibres) delete [] sproc;

  // print configuration
  std::cout << "workers: " << workers << " pthreads: " << threads << " fibres: " << fibres << " locks: " << locks;
  if (yield) std::cout << " yield";
  std::cout << std::endl;
  std::cout << "duration: " << duration << " work: " << work << " unlocked work: " << unlocked << std::endl;

  // collect and print results
  unsigned long long wsum = 0;
  double wsum2 = 0;
  for (unsigned int i = 0; i < workers; i += 1) {
    wsum += winfo[i].counter;
    wsum2 += pow(winfo[i].counter, 2);
  }
  unsigned long long wavg = wsum/workers;
  unsigned long long wstd = (unsigned long long)sqrt(wsum2 / workers - pow(wavg, 2));
  std::cout << "work - total: " << wsum << " rate: " << wsum/duration << " fairness: " << wavg << '/' << wstd << std::endl;

  unsigned long long lsum = 0;
  double lsum2 = 0;
  for (unsigned int i = 0; i < locks; i += 1) {
    lsum += linfo[i].counter;
    lsum2 += pow(linfo[i].counter, 2);
  }
  unsigned long long lavg = lsum/locks;
  unsigned long long lstd = (unsigned long long)sqrt(lsum2 / locks - pow(lavg, 2));
  std::cout << "locks - total " << wsum << " rate: " << wsum/duration << " fairness: " << lavg << '/' << lstd << std::endl;

  // clean up
  if (fibres) {
    delete starter.fbar;
    for (unsigned int i = 0; i < locks; i += 1) delete linfo[i].flock;
  } else {
    for (unsigned int i = 0; i < locks; i += 1) SYSCALL(pthread_mutex_destroy(&linfo[i].plock));
  }

  delete [] linfo;
  delete [] winfo;

  // done
  return 0;
}
