/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _SystemProcessor_h_
#define _SystemProcessor_h_ 1

#include "runtime/Cluster.h"
#include "libfibre/Poller.h"

#include <semaphore.h>

class Fibre;

class SystemProcessor : public Context, public BaseProcessor {
  pthread_t       sysThread;
  SystemSemaphore idleSem;

  inline void   idleLoop();
  static void   idleLoopSetupFibre(void*);
  static void*  idleLoopSetupPthread(void*);

protected:
  // dedicated constructor for C interface using existing pthread
  SystemProcessor(Cluster& cluster, funcvoid1_t func, ptr_t arg);

public:
  // regular constructors
  SystemProcessor();
  SystemProcessor(Cluster& cluster);

  // dedicated constructor for bootstrap
  SystemProcessor(Cluster& cluster, _friend<_Bootstrapper>);
  // dedicated initialization routine for bootstrap
  Fibre* init(_friend<_Bootstrapper>);

  // destructor moves all fibres and ends pthread
  virtual ~SystemProcessor();

  pthread_t getSysID() { return sysThread; }

  virtual void wakeUp() {
    stats->wake.count();
    idleSem.V();
  }
};

static inline SystemProcessor& CurrProcessor() {
  SystemProcessor* proc = Context::self();
  GENASSERT(proc);
  return *proc;
}

class FibreCluster : public Cluster {
  volatile bool   paused;
  SystemSemaphore pauseWaitSem;
  SystemSemaphore pauseContSem;

#if !TESTING_POLLER_FIBRES
  size_t          pollThreshold;
  SystemCondition pollCond;
#endif

public:
  FibreCluster(size_t t) : paused(false), pauseContSem(1)
#if !TESTING_POLLER_FIBRES
  , pollThreshold(t)
#endif
  {}

#if !TESTING_POLLER_FIBRES && !TESTING_POLLER_IDLETIMEDWAIT
  ~FibreCluster() {
    ScopedLock<SystemLock> al(idleLock);
    pollThreshold = 0;
    pollCond.signal();
  }
#endif

  size_t addIdleProcessor(BaseProcessor& proc) {
    if (paused) {
      pauseWaitSem.V();
      pauseContSem.P();
      pauseContSem.V();
    }
    size_t c = Cluster::addIdleProcessor(proc);
#if !TESTING_POLLER_FIBRES && TESTING_POLLER_IDLEWAIT
    if (c == pollThreshold) pollCond.signal();
#endif
    return c;
  }

#if !TESTING_POLLER_FIBRES
  bool waitForPoll() {
#if TESTING_POLLER_IDLEWAIT
    ScopedLock<SystemLock> al(idleLock);
    if (idleCount < pollThreshold) {
#if TESTING_POLLER_IDLETIMEDWAIT
      Time t;
      SYSCALL(clock_gettime(CLOCK_REALTIME, &t));
      pollCond.wait(idleLock, t + Time(0,50000000));    // 50 ms = 50,000,000 ns;
#else
      pollCond.wait(idleLock);
#endif // TESTING_POLLER_IDLETIMEDWAIT
      return true;
    }
#endif // TESTING_POLLER_IDLEWAIT
    return false;
  }
#endif // !TESTING_POLLER_FIBRES

  void resume() {
    paused = false;
    pauseContSem.V();
  }

  void pause() {
    pauseContSem.P();
    paused = true;
    idleLock.acquire();
    BaseProcessor* proc = idleList.front();
    while (proc != idleList.edge()) {
      proc->wakeUp();
      proc = ProcessorList::next(*proc);
    }
    idleLock.release();
    size_t start = (&CurrProcessor().getCluster() == this) ? 1 : 0;
    for (size_t i = start; i < procCount(); i += 1) pauseWaitSem.P();
  }
};

class PollerCluster : public FibreCluster {
  Poller poller;
public:
  PollerCluster(size_t t = 1) : FibreCluster(t), poller(*this) {}
  ~PollerCluster() {
#if TESTING_POLLER_FIBRES
    GENASSERT(poller.stopped() || procCount() > 0);
#endif
  }
  Poller& getPoller() { return poller; }

  void stopPoller() {
#if TESTING_POLLER_FIBRES
    GENASSERT(!poller.stopped() && procCount() > 0);
#endif
    poller.stop();
  }

  void pause() {
    poller.pause();
    FibreCluster::pause();
  }

  void resume() {
    FibreCluster::resume();
    poller.resume();
  }
};

static inline PollerCluster& CurrCluster() {
  return reinterpret_cast<PollerCluster&>(CurrProcessor().getCluster());
}

static inline Poller& CurrPoller() {
  return CurrCluster().getPoller();
}

#endif /* _SystemProcessor_h_ */
